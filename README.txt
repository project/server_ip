CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers

INTRODUCTION
------------

Server IP is a simple module, it will display the ip address of the server. 
Nowadays most of the sites using the cloud servers with more than
one IP address for different region for the same site.
It will be helpful for developers to debug the IP address of cloud server
that contains more than one ip address for single site.

Default Option :

Developers can also view the below details for easy debugging :

 * Database Host Name
 * Database Name
 * Current Theme
 * Path To Theme
 * Base Url

User Select Option :
 User can able to select the PHP $_SERVER variables in the configuration form. 
 The selected variables will be displayed in the server details menu.

More info:

 * For a full description of the module, visit [the project page]
   (https://www.drupal.org/project/server_ip).  

REQUIREMENTS
------------

This module requires no modules outside of Drupal core.

INSTALLATION
------------

 * Install the Server IP module as you would normally install a 
   contributed Drupal module. Visit https://www.drupal.org/node/1897420 
   for further information.    

CONFIGURATION
-------------

* Configure the Server Settings in 
  Administration » Configuration » Server Settings:

1. After install this module, we can see the menu 'Server Details' in the
   admin configuration menu. By clicking this menu,
   we can see the details for debugging.
2. By default, administrator role can access this url. If we want to access
   this url to different role, go to permission page and find the word
   'View Server Details' to set the permission for different role.

MAINTAINERS
-----------

Current maintainers:
  Elavarasan R - https://www.drupal.org/user/1902634
