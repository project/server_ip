<?php

namespace Drupal\server_ip\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Database\Database;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Drupal\Core\Theme\ThemeManager;

/**
 * Controller routines for server_ip routes.
 */
class ServerDetailsController extends ControllerBase {

  /**
   * Configuration object.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $config;

  /**
   * Request object.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $request;

  /**
   * Theme Manager.
   *
   * @var \Drupal\Core\Theme\ThemeManager
   */
  protected $thememanager;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config
   *   The config service.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request
   *   The request service.
   * @param \Drupal\Core\Theme\ThemeManager $thememanager
   *   The theme manager service.
   */
  public function __construct(ConfigFactoryInterface $config,
                              RequestStack $request,
                              ThemeManager $thememanager) {
    $this->config = $config;
    $this->request = $request;
    $this->thememanager = $thememanager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('request_stack'),
      $container->get('theme.manager')
    );
  }

  /**
   * Callback function for server details.
   */
  public function getServerDetails() {
    // Get server variables.
    $server_uri = $this->request->getCurrentRequest()->server;
    $server = [];
    foreach ($server_uri as $key => $value) {
      $server[$key] = $value;
    }
    // Get database information.
    $db = Database::getConnectionInfo();
    // Get configuration variables.
    $arr_config = $this->config->get('server_ip.settings')->get('server');
    $arr_config = empty($arr_config) ? [] : $arr_config;
    // Get current theme name.
    $current_theme = $this->thememanager->getActiveTheme()->getName();
    // Set server related details.
    $hostname = $db['default']['host'];
    $database = $db['default']['database'];
    $server_addr = $server['SERVER_ADDR'];
    $arr_details = [
      'default' => [
        'server_addr' => $server_addr,
        'hostname' => $hostname,
        'database' => $database,
        'theme' => $current_theme,
      ],
    ];

    foreach ($arr_config as $key => $value) {
      $arr_details['server'][$key] = $server[$key];
    }
    return [
      '#theme' => 'server_details',
      '#result' => $arr_details,
    ];
  }

}
