<?php

namespace Drupal\server_ip\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Configure Server variables settings.
 */
class ServerVariableSettingsForm extends ConfigFormBase {

  /**
   * Config settings.
   *
   * @var string
   */
  const SETTINGS = 'server_ip.settings';

  /**
   * Request object.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $request;

  /**
   * Constructor.
   *
   * @param \Symfony\Component\HttpFoundation\RequestStack $request
   *   The request service.
   */
  public function __construct(RequestStack $request) {
    $this->request = $request;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'server_ip_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('request_stack')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Get server variables.
    $arr_server = $this->request->getCurrentRequest()->server;
    $array_list = [];
    foreach ($arr_server as $key => $value) {
      $array_list[$key] = $key;
    }
    $default = $this->config(static::SETTINGS)->get('server');
    $form['server'] = [
      '#weight' => '1',
      '#multiple' => TRUE,
      '#type' => 'select',
      '#options' => $array_list,
      '#title' => 'PHP $_SERVER Variables',
      '#default_value' => $default,
      '#description' => $this->t('Please choose the variables to display'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Retrieve the configuration.
    $arr_server = $form_state->getValue('server');
    $this->config(static::SETTINGS)->delete();
    $config = $this->config(static::SETTINGS);
    foreach ($arr_server as $key => $value) {
      $config->set('server.' . $key, $value);
    }
    $config->save();
    parent::submitForm($form, $form_state);
  }

}
